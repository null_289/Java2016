import java.util.Arrays;


public class ArrayList {
    private Object list[];
    private int next;

    public ArrayList(int i){
        list = new Object[i];
    }
    public ArrayList(){
        this(16);
    }

    public void add(Object o){
        if (next == list.length){
            list = Arrays.copyOf(list,list.length*2);
        }
        list[next++] = o;
    }

    public Object get(int i){
        return list[i];
    }
    public int size(){
        return next;
    }
}