import java.util.Scanner;

/**
 * Created by lsm on 16-3-27.
 */
public class ConsoleGame extends GuessGame  {
    Scanner s = new Scanner(System.in);

    @Override
    public void print(String s) {
        System.out.print(s);
    }

    @Override
    public int nextInt() {
        return s.nextInt();
    }
}