import java.util.Scanner;

public class Guest {
    public static void main(String[] args) {
        ArrayList names = new ArrayList();
        collectNameTo(names);
        System.out.println("访客名单：");
        printUpperCase(names);

    }

    static void collectNameTo(ArrayList names){
        Scanner s = new Scanner(System.in);
        while (true){
            System.out.print("访客名称");
            String name = s.nextLine();
            if (name.equals("quit")){
                break;
            }
            names.add(name);
        }
    }

    static void printUpperCase(ArrayList names){
        for (int i = 0;i < names.size();i++){
            String name = (String) names.get(i);
            System.out.println(name.toUpperCase());
        }
    }
}