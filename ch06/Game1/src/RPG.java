﻿
public class RPG {
    public static void main(String[] args) {
        demoSwordsMan();
        demoMagician();
    }

    static void demoMagician() {
        Magician magician = new Magician();
        magician.setName("Monica");
        magician.setLevel(1);
        magician.setBlood(100);
        System.out.println("魔法师："+"("+magician.getName()+" "+magician.getLevel()+" "+magician.getBlood()+")");
    }

    static void demoSwordsMan(){
        SwordsMan swordsman = new SwordsMan();
        swordsman.setName("Justin");
        swordsman.setLevel(1);
        swordsman.setBlood(200);
        System.out.println("剑士："+"("+swordsman.getName()+" "+swordsman.getLevel()+" "+swordsman.getBlood()+")");

    }
}
