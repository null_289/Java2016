

import java.util.*;

class StringComparator implements Comparator<String> {
    @Override
    public int compare(String s1, String s2) {
        return -s1.compareTo(s2);
    }
}

public class Messages3 {
    public static void main(String[] args) {
        Map<String, String> messages = new TreeMap<>(new StringComparator()); 
        messages.put("Justin", "Hello！Justin的信息！");
        messages.put("Monica", "给Monica的悄悄话！");
        messages.put("Irene", "Irene的可爱猫喵喵叫！");
        System.out.println(messages);
    }
}
