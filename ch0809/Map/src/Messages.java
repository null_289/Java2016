

import java.util.*;

public class Messages {
    public static void main(String[] args) {
        Map<String, String> messages = new HashMap<>(); 
        messages.put("Justin", "Hello！Justin的信息！");
        messages.put("Monica", "给Monica的悄悄话！");
        messages.put("Irene", "Irene的可爱猫喵喵叫！");
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("取得谁的信息：");
        String message = messages.get(scanner.nextLine());
        System.out.println(message);
        System.out.println(messages);
    }
}
